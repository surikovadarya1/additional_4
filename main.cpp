﻿#include <BMP.hpp>
#include <iostream>

int main()
{
    try
    {
        mt::images::BMP test_bmp(1080, 720);
        test_bmp.Fill({ 0,0,0 });
        test_bmp.Message(200, 100);
        test_bmp.Save("test.bmp");
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}